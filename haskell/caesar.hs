import Data.Char

--simple Caesar encoding as specified in Hutton, "Programming in Haskell"
--usage: encode 2 "hello world" -> "jgnnq yqtnf"

char2int :: Char->Int
char2int c = ord c - ord 'a'

int2char :: Int->Char
int2char n = chr (ord 'a' + n)

shift :: Int->Char->Char
shift n c | isLower c = int2char ((char2int c + n) `mod` 26)
          | otherwise = c

encode :: Int -> String -> String
encode n xs = [shift n x | x <- xs ]
